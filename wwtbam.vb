Module Main

    Class Answer

        'Define the properties of an answer

        Public answer As String = ""
        Public correct As Boolean = False

    End Class

    Class Question

        'Define the properties of a question

        Public name As String = ""
        Public answers(3) As Answer

        Public Sub Init()

            For i = 0 To 3

                'Create 4 questions

                answers(i) = New Answer

            Next

        End Sub

    End Class

    Dim blnUsedAskAudience As Boolean
    Dim blnUsedPhoneFriend As Boolean
    Dim blnUsed5050 As Boolean

    Function SetupQuestions()

        blnUsedAskAudience = False
        blnUsedPhoneFriend = False
        blnUsed5050 = False

        'Initiate an array of questions

        Dim questions(11) As Question

        For i = 0 To questions.Length - 1

            questions(i) = New Question
            questions(i).Init()

        Next

        'Define the questions

        questions(0).name = "Translate this binary 001010 into text"
        questions(0).answers(0).answer = "22"
        questions(0).answers(1).answer = "10"
        questions(0).answers(1).correct = True
        questions(0).answers(2).answer = "hi"
        questions(0).answers(3).answer = "99"

        questions(1).name = "What letter must appear at the beginning of the registration number of all non-military aircraft in the U.S.?"
        questions(1).answers(0).answer = "N"
        questions(1).answers(0).correct = True
        questions(1).answers(1).answer = "A"
        questions(1).answers(2).answer = "U"
        questions(1).answers(3).answer = "L"

        questions(2).name = "which one is better a RTX 2080Ti or a GTX TitanV?"
        questions(2).answers(0).answer = "RTX 2080Ti"
        questions(2).answers(1).answer = "EQUAL"
        questions(2).answers(2).answer = "they are both trash"
        questions(2).answers(3).answer = "Titan v"
        questions(2).answers(3).correct = True

        questions(3).name = "What is the latest gen on intel processor?"
        questions(3).answers(0).answer = "9st gen"
        questions(3).answers(0).correct = True
        questions(3).answers(1).answer = "8th gen"
        questions(3).answers(2).answer = "10th gen "
        questions(3).answers(3).answer = "4th gen"

        questions(4).name = "?"
        questions(4).answers(0).answer = ""
        questions(4).answers(0).correct = True
        questions(4).answers(1).answer = ""
        questions(4).answers(2).answer = ""
        questions(4).answers(3).answer = ""

        questions(5).name = "?"
        questions(5).answers(0).answer = ""
        questions(5).answers(1).answer = ""
        questions(5).answers(2).answer = ""
        questions(5).answers(2).correct = True
        questions(5).answers(3).answer = ""

        questions(6).name = "?"
        questions(6).answers(0).answer = ""
        questions(6).answers(1).answer = ""
        questions(6).answers(2).answer = ""
        questions(6).answers(3).answer = ""
        questions(6).answers(3).correct = True

        questions(7).name = "?"
        questions(7).answers(0).answer = ""
        questions(6).answers(0).correct = True
        questions(7).answers(1).answer = ""
        questions(7).answers(2).answer = ""
        questions(7).answers(3).answer = ""

        questions(8).name = "?"
        questions(8).answers(0).answer = ""
        questions(8).answers(1).answer = ""
        questions(8).answers(1).correct = True
        questions(8).answers(2).answer = ""
        questions(8).answers(3).answer = ""

        questions(9).name = "?"
        questions(9).answers(0).answer = ""
        questions(9).answers(0).correct = True
        questions(9).answers(1).answer = ""
        questions(9).answers(2).answer = ""
        questions(9).answers(3).answer = ""

        questions(10).name = "?"
        questions(10).answers(0).answer = ""
        questions(10).answers(1).answer = ""
        questions(10).answers(2).answer = ""
        questions(10).answers(2).correct = True
        questions(10).answers(3).answer = ""

        questions(11).name = "?"
        questions(11).answers(0).answer = ""
        questions(11).answers(0).correct = True
        questions(11).answers(1).answer = ""
        questions(11).answers(2).answer = ""
        questions(11).answers(3).answer = ""

        Return questions

    End Function

    Sub Main()

        'Start the main menu

        Menu()

    End Sub

    Sub Menu()


        Dim strResponse As String = ""
        Dim blnAskedOnce As Boolean = False

        'Keep on looping until the users says me or not me

        Do

            'Ask who wants to be a millionaire, if it has been shown more than once show a hint

            If (blnAskedOnce) Then

                Console.WriteLine("Who wants to be a millionaire? (Me or Not me)")

            Else

                Console.WriteLine("Who wants to be a millionaire?")

            End If

            blnAskedOnce = True

            strResponse = Console.ReadLine().Trim().ToLower()

            Console.Clear()

        Loop While (Not (strResponse = "me" Or strResponse = "not me"))

        'If the response was me show the rules then start the game, if not then end the program

        If (strResponse = "me") Then

            showRules()
            showQuestions()

        Else

            Exit Sub

        End If

    End Sub

    Sub showRules()

        'Define the rules

        Dim strRules() As String = {"The first two questions are worth £500 and £1,000.", "The next five questions are worth £2,000, £5,000, £10,000, £20,000 and £50,000.", "The final five questions are worth £75,000, £150,000, £250,000, £500,000 and £1,000,000.", "There are 3 lifelines, you can ask the audience, phone a friend or have a 50:50."}

        Console.WriteLine("Rules")
        Console.WriteLine("========" & vbNewLine)

        'Show one line of the rules every key press

        For i = 0 To strRules.Length() - 1

            Console.WriteLine((i + 1).ToString() & ": " & strRules(i))
            Console.ReadKey()

        Next

        Console.WriteLine(vbNewLine & "Press any key to start the game")
        Console.ReadKey()

    End Sub

    Sub showQuestions()

        'Declare the main quiz variables

        Dim questions() As Question = SetupQuestions()
        Dim strPrizes() As String = {"£500", "£1000", "£2000", "£5000", "£10000", "£20000", "£50000", "£75000", "£150000", "£250000", "£500000", "£1000000"}
        Dim strCorrectAns As String

        'Loop through the questions

        For i = 0 To questions.Length() - 1

            'Display the current question

            Console.Clear()

            Console.WriteLine("Question #" & (i + 1).ToString() & ": ")
            Console.WriteLine(questions(i).name)
            Console.WriteLine("=============")

            Console.WriteLine(vbNewLine & "A: " & questions(i).answers(0).answer)
            Console.WriteLine("B: " & questions(i).answers(1).answer)
            Console.WriteLine("C: " & questions(i).answers(2).answer)
            Console.WriteLine("D: " & questions(i).answers(3).answer)

            'Handle the answer, if it was incorrect tell the user the correct answer and go back to the menu, if not then tell the user it was right and continue

            If Not handleAnswer(questions, questions(i)) Then

                'Find which answer was the correct one

                For j = 0 To questions(i).answers.Length - 1

                    If questions(i).answers(j).correct Then

                        strCorrectAns = questions(i).answers(j).answer

                    End If

                Next

                Console.WriteLine("You got the question wrong, the correct answer was " & strCorrectAns & ". Better luck next time!")
                Console.ReadKey()
                Exit For

            Else

                'If the current question is the last tell the user they won 1 million

                If i = questions.Length() - 1 Then

                    Console.WriteLine("Well done, you have won " & strPrizes(strPrizes.Length - 1) & "!")
                    Console.ReadKey()
                    Exit For

                End If

            End If

            Console.WriteLine("Well done, you have won " & strPrizes(i) & "!")
            Console.ReadKey()

        Next

        Console.Clear()
        Menu()

    End Sub

    Function handleAnswer(questions, question)

        Dim strUserAnswer As String

        'Repeat until an answer is recieved

        While True

            Console.Write(vbNewLine & "Enter your answer or use a lifeline: ")
            strUserAnswer = Console.ReadLine()
            strUserAnswer = strUserAnswer.ToLower().Trim()

            'Check if the user's answer was correct or if they asked for a lifeline

            If strUserAnswer = "a" Or strUserAnswer = "b" Or strUserAnswer = "c" Or strUserAnswer = "d" Then

                If strUserAnswer = "a" And question.answers(0).correct Then

                    Return True

                ElseIf strUserAnswer = "b" And question.answers(1).correct Then

                    Return True

                ElseIf strUserAnswer = "c" And question.answers(2).correct Then

                    Return True

                ElseIf strUserAnswer = "c" And question.answers(3).correct Then

                    Return True

                Else

                    Return False

                End If

            ElseIf strUserAnswer = "lifeline" Then

                'Send it to the lifeline handler if the user asks for the lifeline

                Dim intLifeLineStatus As Integer = lifeLineHandler(questions, question)

                If intLifeLineStatus = 1 Then

                    Return True

                ElseIf intLifeLineStatus = 2 Then

                    Return False

                End If

            End If

        End While

    End Function

    Function lifeLineHandler(questions, question)

        Dim strAnswer As String
        Dim strUserAnswer As String

        Do

            Console.WriteLine("Which lifeline do you want to use? (Type exit to go back)")
            strAnswer = Console.ReadLine().ToLower().Trim()

        Loop Until strAnswer = "ata" Or strAnswer = "ask the audience" Or strAnswer = "ask audience" Or strAnswer = "paf" Or strAnswer = "phone a friend" Or strAnswer = "phone friend" Or strAnswer = "5050" Or strAnswer = "50:50" Or strAnswer = "50/50" Or strAnswer = "50 50" Or strAnswer = "exit"

        If (strAnswer = "ata" Or strAnswer = "ask the audience" Or strAnswer = "ask audience") And (Not blnUsedAskAudience) Then

            'Set ask the audience as used

            blnUsedAskAudience = True

            'Get the results from the audience then ask for an answer again

            Dim intAudienceAnswers() As UShort = askAudience(question.answers)

            Console.WriteLine(vbNewLine & "The audience have voted, the results are:")
            Console.WriteLine("A: " & intAudienceAnswers(0) & "%")
            Console.WriteLine("B: " & intAudienceAnswers(1) & "%")
            Console.WriteLine("C: " & intAudienceAnswers(2) & "%")
            Console.WriteLine("D: " & intAudienceAnswers(3) & "%")

            While True

                Console.Write(vbNewLine & "Enter your answer: ")
                strUserAnswer = Console.ReadLine()
                strUserAnswer = strUserAnswer.ToLower().Trim()

                'Check if the user's answer was correct

                If strUserAnswer = "a" Or strUserAnswer = "b" Or strUserAnswer = "c" Or strUserAnswer = "d" Then

                    If strUserAnswer = "a" And question.answers(0).correct Then

                        Return 1

                    ElseIf strUserAnswer = "b" And question.answers(1).correct Then

                        Return 1

                    ElseIf strUserAnswer = "c" And question.answers(2).correct Then

                        Return 1

                    ElseIf strUserAnswer = "c" And question.answers(3).correct Then

                        Return 1

                    Else

                        Return 2

                    End If

                End If

            End While

        ElseIf (strAnswer = "paf" Or strAnswer = "phone a friend" Or strAnswer = "phone friend") And (Not blnUsedPhoneFriend) Then

            'Set phone a friend as used

            blnUsedPhoneFriend = True

            'Get the results from phoning a friend then ask for an answer again

            Dim intRandomAnswer As Integer = phoneAFriend()
            Dim strCorrectAns As String

            If intRandomAnswer = 1 Then

                strCorrectAns = "A"

            ElseIf intRandomAnswer = 2 Then

                strCorrectAns = "B"

            ElseIf intRandomAnswer = 3 Then

                strCorrectAns = "C"

            Else

                strCorrectAns = "D"

            End If

            Console.WriteLine(vbNewLine & "Your friend says the answer is " & strCorrectAns)

            While True

                Console.Write(vbNewLine & "Enter your answer: ")
                strUserAnswer = Console.ReadLine()
                strUserAnswer = strUserAnswer.ToLower().Trim()

                'Check if the user's answer was correct

                If strUserAnswer = "a" Or strUserAnswer = "b" Or strUserAnswer = "c" Or strUserAnswer = "d" Then

                    If strUserAnswer = "a" And question.answers(0).correct Then

                        Return 1

                    ElseIf strUserAnswer = "b" And question.answers(1).correct Then

                        Return 1

                    ElseIf strUserAnswer = "c" And question.answers(2).correct Then

                        Return 1

                    ElseIf strUserAnswer = "c" And question.answers(3).correct Then

                        Return 1

                    Else

                        Return 2

                    End If

                End If

            End While

        ElseIf (strAnswer = "5050" Or strAnswer = "50:50" Or strAnswer = "50/50" Or strAnswer = "50 50") And (Not blnUsed5050) Then

            'Set 50:50 as used

            blnUsed5050 = True

            'Get the 2 results from 5050

            Dim newAnswers() As Answer = halfs(question.answers)

            While True

                Console.WriteLine("The new answers are:")
                Console.WriteLine("A: " & newAnswers(0).answer)
                Console.WriteLine("B: " & newAnswers(1).answer)

                Console.Write(vbNewLine & "Enter your answer: ")
                strUserAnswer = Console.ReadLine()
                strUserAnswer = strUserAnswer.ToLower().Trim()

                'Check if the user's answer was correct

                If strUserAnswer = "a" Or strUserAnswer = "b" Then

                    If strUserAnswer = "a" And newAnswers(0).correct Then

                        Return 1

                    ElseIf strUserAnswer = "b" And newAnswers(1).correct Then

                        Return 1

                    Else

                        Return 2

                    End If

                End If

            End While

        ElseIf strAnswer = "exit" Then

            Return 3

        End If

    End Function

    Function askAudience(answers)


        Dim intAudienceAnswers(3) As UShort
        Dim intTemp As UShort

        Console.Write("Getting answers")

        For i = 1 To 3

            Threading.Thread.Sleep(500)
            Console.Write(".")

        Next

        'Get 3 random results that add up to 100

        Do

            For i = 0 To 3

                Randomize()
                intAudienceAnswers(i) = (100 * Rnd()) + 1

            Next


        Loop Until (intAudienceAnswers(0) + intAudienceAnswers(1) + intAudienceAnswers(2) + intAudienceAnswers(3) = 100)

        Array.Sort(intAudienceAnswers)

        For i = 0 To 3

            If answers(i).correct Then

                If (i = 0) Then

                    intTemp = intAudienceAnswers(i)
                    intAudienceAnswers(i) = intAudienceAnswers(i - 1)
                    intAudienceAnswers(i - 1) = intTemp

                Else

                    intTemp = intAudienceAnswers(i)
                    intAudienceAnswers(i) = intAudienceAnswers(i + 1)
                    intAudienceAnswers(i + 1) = intTemp

                End If

            End If

        Next

        Return intAudienceAnswers

    End Function

    Function phoneAFriend()

        Console.Write("Calling")

        For i = 1 To 3

            System.Threading.Thread.Sleep(500)
            Console.Write(".")

        Next

        'Give the user a random answer

        Return randomAnswer()

    End Function

    Function halfs(answers)

        Dim intRandomAnswer As Integer
        Dim newAnswers(1) As Answer
        Dim tempAnswer As Answer

        'Get two random answers, one of them being the correct one and remove the other two

        Do

            intRandomAnswer = randomAnswer() - 1

        Loop Until (Not answers(intRandomAnswer).correct)

        newAnswers(0) = answers(intRandomAnswer)

        Do

            intRandomAnswer = randomAnswer() - 1

        Loop Until (answers(intRandomAnswer).correct)

        newAnswers(1) = answers(intRandomAnswer)


        'Swap the answers at a random chance

        Randomize()
        intRandomAnswer = Int((2 * Rnd()) + 1)

        If (intRandomAnswer = 2) Then

            tempAnswer = newAnswers(1)
            newAnswers(1) = newAnswers(0)
            newAnswers(0) = tempAnswer

        End If

        Return newAnswers

    End Function

    Function randomAnswer()

        Dim intRandom As UShort

        'Generate a random number from 1 to 4

        Randomize()
        intRandom = Int((4 * Rnd()) + 1)

        Return intRandom

    End Function

End Module
